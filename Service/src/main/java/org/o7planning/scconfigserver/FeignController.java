package org.o7planning.scconfigserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class FeignController {
@Autowired
private FeignConsume feignConsume;
@RequestMapping("/phrase")
@HystrixCommand(fallbackMethod = "defaultName")
String getPhrase() {
  return "my name is "+feignConsume.getConsumedName();
  }
public String defaultName()
{
	return "this is the back up you are wishing for ";
	
}
}
