package org.o7planning.scconfigserver;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name="Service2")
public interface FeignConsume {
	@RequestMapping("/name")
	String  getConsumedName();
}
