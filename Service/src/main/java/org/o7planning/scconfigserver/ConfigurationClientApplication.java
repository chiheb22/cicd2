package org.o7planning.scconfigserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@SpringBootApplication
@EnableCircuitBreaker
public class ConfigurationClientApplication {
	
	
  public static void main(String[] args) {
    SpringApplication.run(ConfigurationClientApplication.class, args);
  }
}
@EnableFeignClients
@RefreshScope
@RestController

class MessageRestController {

  @Value("${message:Hello default}")
  private String message;
    
    @RequestMapping("/message")
    
  String getMessage() {
    return this.message;
  }
    
}